#!/usr/bin/env python3

from urllib.request import urlopen, URLError
from tempfile import mkdtemp, mkstemp
from stat import *
import os, os.path, sys, subprocess, shutil, atexit

for cmd in ["nix-build","patchelf"]:
    if shutil.which(cmd) is None:
        print("no " + cmd + " command found in PATH")
        sys.exit(1)

tmpdir = mkdtemp()

def cleanup(tmpdir):
    shutil.rmtree(tmpdir)

def bootstrap():
    try:
        mode = os.stat("ruleset_converter").st_mode
        if not mode & S_IXUSR:
            os.chmod("ruleset_converter", mode | S_IXUSR)
        return
    except FileNotFoundError:
        pass

    buf = urlopen("https://github.com/bromite/filters/tags").read()
    for line in buf.split(b'\n'):
        if b'a href="/bromite/filters/releases/tag/' in line:
            i = line.find(b"tag/") + 4
            j = line.find(b'">')
            version = line.decode()[i:j]
            break

    buf = urlopen("https://github.com/bromite/filters/releases/download/"+version+"/ruleset_converter").read()
    with open("ruleset_converter", "wb") as f:
        f.write(buf)

    glibc = subprocess.check_output(["nix-build","--no-out-link","<nixpkgs>","-A","glibc"]).rstrip().decode()
    subprocess.run(["patchelf","--set-interpreter",glibc+"/lib64/ld-linux-x86-64.so.2",
                    "ruleset_converter"])

    mode = os.stat("ruleset_converter").st_mode
    os.chmod("ruleset_converter", mode | S_IXUSR)

def main():
    tmps = []
    with open("lists.txt") as f:
        for line in f:
            url = line.rstrip()
            try:
                buf = urlopen(url).read()
            except URLError:
                print("failed downloading")
                sys.exit(1)
            tmp = mkstemp(dir=tmpdir)
            tmps.append(tmp[1])
            os.write(tmp[0],buf)

    input_files = ",".join(tmps)
    try:
        os.remove("filters.dat")
    except FileNotFoundError:
        pass
    subprocess.run(["./ruleset_converter", "--input_format=filter-list",
                    "--output_format=unindexed-ruleset",
                    "--input_files="+input_files,
                    "--output_file=filters.dat"])

atexit.register(cleanup, tmpdir)

bootstrap()
main()
